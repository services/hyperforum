<?php

require_once('../config.php');

printf(
'<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JavaScript license information</title>
    </head>
    <body>
        <table id="jslicense-labels1">
            <tr>
                <td><a href="'.$base_url.'/style/Hyperforum/responsive-nav.min.js">responsive-nav.min.js</a></td>
                <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
                <td><a href="https://github.com/viljamis/responsive-nav.js/archive/1.0.39.tar.gz">responsive-nav.tar.gz</a></td>
            </tr>
            <tr>
                <td><a href="'.$base_url.'/include/js/min/punbb.common.min.js">punbb.common.min.js</a></td>
                <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
                <td><a href="'.$base_url.'/include/js/punbb.common.js">punbb.common.js</a></td>
            </tr>
            <tr>
                <td><a href="'.$base_url.'/include/js/min/punbb.timezone.min.js">punbb.timezone.min.js</a></td>
                <td><a href="https://www.gnu.org/licenses/gpl-2.0.html">GNU-GPL-2.0</a></td>
                <td><a href="'.$base_url.'/include/js/punbb.timezone.js">punbb.timezone.js</a></td>
            </tr>
            <tr>
                <td><a href="'.$base_url.'/include/js/min/punbb.install.min.js">punbb.install.min.js</a></td>
                <td><a href="https://www.gnu.org/licenses/gpl-2.0.html">GNU-GPL-2.0</a></td>
                <td><a href="'.$base_url.'/include/js/punbb.install.js">punbb.install.js</a></td>
            </tr>
            <tr>
                <td><a href="'.$base_url.'/include/js/LAB.src.js">LAB.src.js</a></td>
                <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
                <td><a href="'.$base_url.'/include/js/LAB.src.js">LAB.src.js</a></td>
            </tr>
        </table>
    </body>
</html>
');
